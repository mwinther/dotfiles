# Set default shell
set-option -g default-command 'zsh -i'
set-option -g default-terminal 'screen-256color'
set-option -ga terminal-overrides ",screen-256color:Tc"
set-option -g update-environment 'MW_DEBUG SSH_AGENT_PID SSH_AUTH_SOCK'

# Set prefix to .
unbind C-b
set -g prefix C-w

# History limit
set -g history-limit 10000

# Start windows and panes at 1
set -g base-index 1
set -g pane-base-index 1

# Session hopping
bind s choose-session

# Set colors
set-option -g status-bg colour23
set-option -g status-fg colour251
set-option -g pane-border-style fg=colour23
set-option -g pane-active-border-style fg=colour36

# Set window name in status bar to last pane title
set -g window-status-format '#I:[#T]#F'
set -g window-status-current-format '#I:[#T]#F'
set-window-option -g automatic-rename off

# Set format for the right section of the status bar
set-option -g status-right "%a %F %H:%M "

# Copy mode
set-window-option -g mode-keys vi
bind C-v copy-mode
bind -T copy-mode-vi 'y' send -X copy-selection
bind -T copy-mode-vi 'v' send -X begin-selection

# Buffer operations
bind C-f choose-buffer 'run-shell "tmux show-buffer -b %% | cut > /dev/null'
bind C-m choose-buffer 'run-shell "tmux show-buffer -b %% | mailx -s \"Buffer content `date`\" $USER"'

# Move between panes with Alt-hjkl (no prefix)
bind -n M-k select-pane -U
bind -n M-j select-pane -D
bind -n M-l select-pane -R
bind -n M-h select-pane -L

# Resize panes with Alt-Shift-hjkl (no prefix)
bind -n M-K resize-pane -U
bind -n M-J resize-pane -D
bind -n M-H resize-pane -R
bind -n M-L resize-pane -L

# Split window with Ctrl-Alt-hjkl (no prefix)
bind -n C-M-k split-window -bc "#{pane_current_path}"
bind -n C-M-j split-window -c "#{pane_current_path}"
bind -n C-M-h split-window -bhc "#{pane_current_path}"
bind -n C-M-l split-window -hc "#{pane_current_path}"

# What to put on Ctrl-Alt-Shift?
# bind -n C-M-S-k split-window -bc "#{pane_current_path}"

# Miscellaneous stuff
bind r source-file ~/.tmux.conf
set -s escape-time 0
set -g focus-events on
